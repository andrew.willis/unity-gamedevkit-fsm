/*
Code taken from Jason Weimann.
*/

namespace UMNP.GamedevKit.FSM
{
    public interface IState
    {
        void Tick();
        void OnEnter();
        void OnExit();
    }
}
